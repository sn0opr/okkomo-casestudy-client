import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  overviewData
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.overviewData = this.route.snapshot.data['overview']
  }

}
