import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators/catchError';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @Input() newUser = {};
  registered: boolean = false;
  registerationError = false;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    this.http.post('register', JSON.stringify(this.newUser), {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
    .subscribe(
      data => {
        console.log('Registration successful', data);
        alert('You are registered successfully, you can login now.');
        this.router.navigate(['/login']);
      },
      error => {
        console.error('Registration Error', error);
        alert('Registration error.');
      }
    )
  }

  get diagnostic() { return JSON.stringify(this.newUser); }

  diagnose(value: any): string {
    return JSON.stringify(value);
  }

}
