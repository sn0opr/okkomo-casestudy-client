import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { JwtModule, JwtInterceptor } from '@auth0/angular-jwt';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { AppComponent } from './app.component';
import { AuthService } from './auth.service';
import { RegisterComponent } from './register/register.component';

import { AppRoutingModule } from './app-routing.module';
import { EqualToValidatorDirective } from './equal-to-validator.directive';
import { AppApiHttpInterceptor } from './shared/AppApiHttpInterceptor';
import { APP_API_CONFIG, APP_CONFIG } from './shared/AppConfig';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './guard/auth.guard';
import { MustBeNotLoggedGuard } from './guard/mustBeNotLogged.guard';
import { DashboardResolve } from './shared/RouteResolvers/dashboard.resolve';
import { FrontComponent } from './front/front.component';
import { OverviewComponent } from './overview/overview.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    EqualToValidatorDirective,
    LoginComponent,
    DashboardComponent,
    FrontComponent,
    OverviewComponent,
    WelcomeComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        headerName: 'Authorization',
        authScheme: 'Bearer ',
        tokenGetter: () => {
          return localStorage.getItem('access-token')
        },
        whitelistedDomains: [
          'localhost:3000'
        ]
      }
    }),
    AppRoutingModule
  ],
  providers: [
    {
      provide: APP_CONFIG,
      useValue: APP_API_CONFIG
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppApiHttpInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    AuthGuard,
    MustBeNotLoggedGuard,
    DashboardResolve,
    AuthService,
    AppApiHttpInterceptor
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
