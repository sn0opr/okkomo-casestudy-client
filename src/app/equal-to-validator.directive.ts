import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS ,Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[equalTo]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: EqualToValidatorDirective, multi: true}
  ]
})
export class EqualToValidatorDirective implements Validator{
  @Input() equalTo: any;
  validate(c: AbstractControl): { [key: string]: any; } {
    return this.equalTo == c.value ? null : {'equalTo': {value: c.value}};
  }
  registerOnValidatorChange?(fn: () => void): void {
  }

  constructor() { }

}
