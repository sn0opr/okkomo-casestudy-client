import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-front',
  templateUrl: './front.component.html',
  styleUrls: ['./front.component.css']
})
export class FrontComponent implements OnInit {

  user = null;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.user = this.authService.getUserData();
  }

}
