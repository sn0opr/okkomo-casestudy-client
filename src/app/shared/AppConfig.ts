import { InjectionToken } from '@angular/core';

export const APP_API_CONFIG = {
    endpoint: {
        scheme: 'http',
        host: 'localhost',
        port: '3000',
        root: 'api/v1'
    }
}

export const APP_CONFIG = new InjectionToken<any>('app.config');