import { Injectable, Inject } from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import { APP_CONFIG } from './AppConfig';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class AppApiHttpInterceptor implements HttpInterceptor {
    
    readonly apiUrl: string;

    constructor(@Inject(APP_CONFIG) config) {
        this.apiUrl = `${config.endpoint.scheme}://${config.endpoint.host}:${config.endpoint.port}/${config.endpoint.root}`;        
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const apiRequest = req.clone({url: `${this.apiUrl}/${req.url}`});
        return  next.handle(apiRequest)
        .catch(error => { 
          if (error instanceof HttpErrorResponse) {
              if (error.error['message']) {
                  alert(error.error['message']);
              }
          } else {
              alert('An error occured from server');
          }
          return Observable.throw(error);
        });
    }
}