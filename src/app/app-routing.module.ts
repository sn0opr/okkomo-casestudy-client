import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './guard/auth.guard';
import { MustBeNotLoggedGuard } from './guard/mustBeNotLogged.guard';
import { DashboardResolve } from './shared/RouteResolvers/dashboard.resolve';
import { FrontComponent } from './front/front.component';
import { OverviewComponent } from './overview/overview.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

const routes: Routes = [
  { path: '', component: FrontComponent, children: [
    { path: '', component: WelcomeComponent, pathMatch: 'full'},
    { path: 'register', component: RegisterComponent, pathMatch: 'full', canActivate: [MustBeNotLoggedGuard] },
    { path: 'login', component: LoginComponent, pathMatch: 'full', canActivate: [MustBeNotLoggedGuard] },
    { path: 'forgot-password', component: ForgotPasswordComponent, pathMatch: 'full', canActivate: [MustBeNotLoggedGuard]},
    { path: 'reset-password', component: ResetPasswordComponent, pathMatch: 'full', canActivate: [MustBeNotLoggedGuard]}
  ]},
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], children: [
    { path: '', redirectTo: 'overview', pathMatch: 'full'},
    { path: 'overview', component: OverviewComponent, resolve: {overview: DashboardResolve}}
  ] }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
