import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) {}

  login(username: string, password: string): Promise<boolean> {
    return this.http.post('login', {username, password})
    .map(data => {
      localStorage.setItem('access-token', data['token']);
      return true;
    }).toPromise();
  }

  logout() {
    if (this.isLoggedIn()) {
      localStorage.clear();
    }
  }

  requestPasswordReset(email: string): Promise<void> {
    return this.http.post('reset/request', {email})
    .map(_ => {return;})
    .toPromise<void>();
  }

  resetPassword(token: string, newPassword: string): Promise<string> {
    return this.http.post('reset/do', {token, newPassword})
    .map(data => data['message'])
    .toPromise();
  }

  isLoggedIn(): boolean {
    if (!this.jwtHelper.tokenGetter() || this.jwtHelper.isTokenExpired())
      return false;
    return true;
  }

  getUserData() {
    if (!this.isLoggedIn())
      return null;
    return this.jwtHelper.decodeToken();
  }
}
